<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Empleados */

//$this->title = 'Update Empleados: ' . $model->id;
$this->title = "Datos del Empleado >> Agregar Datos";
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar Empleado';
?>
<div class="empleados-update col-md-8 col-md-offset-2">

    <h2><?= Html::encode($this->title) ?></h2>
    <hr/>
</div>

<div class="empleados-create">   
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
