<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Empleados */

$this->title = 'Crear Empleados';
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleados-create col-md-8 col-md-offset-2">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr/>
</div>

<div class="empleados-create">    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
