<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Cargos;
use app\models\Estados;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Empleados */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empleados-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="col-md-8 col-md-offset-2">
        <div class="row">
            
            <div class="col-md-6">
                <div class="form-group">
                    <?= $form->field($model, 'codigo')->textInput() ?>
                </div>
            </div>
        

        
            <div class="col-md-6">
                <div class="form-group">
                    <?= $form->field($model, 'nombre')->textInput() ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?= $form->field($model, 'lugar_nacimiento')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                         
                <?php echo $form->field($model,'fecha_nacimiento')->
                    widget(DatePicker::className(),[
                        'dateFormat' => 'yyyy-MM-dd',
                        'clientOptions' => [
                            'yearRange' => '-115:+0',
                            'changeYear' => true
                        ],
                        'options' => ['class' => 'form-control']
                    ]) 
                ?>
                    <?php //= $form->field($model, 'fecha_nacimiento')->textInput() ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>    

        <div class="row">
            <div class="col-md-4">
                <!-- DROP DOWN LIST PARA SELECCION DEL MUNICIPIO SEGUN EL MUNICIPIO --> 
                <div class="form-group">
                    <?= $form->field($model, 'id_cargo')->dropDownList(
                        ArrayHelper::map(Cargos::find()->all(), 'id', 'nombre'),
                        [
                            'class'=>'form-control',
                            'prompt'=>'[-- Seleccione Cargo --]',
                            'for'=>'disabledTextInput',
                            'id'=>'select-cargos'
                        ]
                    )?>
                </div>                
            </div>
            <div class="col-md-2" style="margin-top:25px">
                <?= Html::a('<span class="glyphicon glyphicon-plus-sign"></span> Cargo', ['cargos/create'], ['class'=>'btn btn-default']) ?>
                <!--<button class="btn btn-default"><span class="glyphicon glyphicon-plus-sign"></span> Cargo </button>-->
            </div>    


            <div class="col-md-4">
                <!-- DROP DOWN LIST PARA SELECCION DEL MUNICIPIO SEGUN EL MUNICIPIO --> 
                <div class="form-group">
                    <?= $form->field($model, 'id_estado')->dropDownList(
                        ArrayHelper::map(Estados::find()->where(['activo' => 1])->all(), 'id', 'nombre'),
                        [
                            'class'=>'form-control',
                            'prompt'=>'[-- Seleccione Estado --]',
                            'for'=>'disabledTextInput',
                            'id'=>'select-estados'
                        ]
                    )?>
                </div>                
            </div>
            <div class="col-md-2" style="margin-top:25px">
                <?= Html::a('<span class="glyphicon glyphicon-plus-sign"></span> Estado', ['estados/create'], ['class'=>'btn btn-default']) ?>
                <!--<button class="btn btn-default"><span class="glyphicon glyphicon-plus-sign"></span> Estado </button>-->
            </div>  
        </div>                    

        <?php //= $form->field($model, 'id_cargo')->textInput() ?>

        <?php //= $form->field($model, 'id_estado')->textInput() ?>

        <?php //= $form->field($model, 'fecha_registro')->textInput() ?>

        <div class="form-group">
            <?= Html::submitButton('Guardar Empleado', ['class' => 'btn btn-success']) ?>
        </div>
    
    </div>
    <?php ActiveForm::end(); ?>

</div>
