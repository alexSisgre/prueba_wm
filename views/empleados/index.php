<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmpleadosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Empleados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleados-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Empleados', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'codigo',
                'label' => 'Codigo',
                'value' => 'codigo',
                'contentOptions' => function($model){
                    return ['style' => 'width:100px' ];
                },
                'headerOptions' => ['class' => 'gridViewEmpleadosHeader'],
            ],
            'nombre',
            'lugar_nacimiento',
            //'fecha_nacimiento',
            [
                'attribute' => 'fecha_nacimiento',
                'label' => 'Fecha Nacimiento',
                'value' => function ($data) {
                    return substr($data->fecha_nacimiento,0,10); 
                },
                'contentOptions' => ['class' => 'gridViewEmpleados'],
                'headerOptions' => ['class' => 'gridViewEmpleadosHeader'],
            ],
            

            //'direccion',
            'telefono',
            //'id_cargo',
            [
                'attribute' => 'cargo_nombre',
                'label' => 'cargo',
                'value' => 'cargo.nombre',
                'contentOptions' => ['class' => 'gridViewEmpleados'],
                'headerOptions' => ['class' => 'gridViewEmpleadosHeader'],
            ],
            [
                'attribute' => 'estado_nombre',
                'label' => 'Estado',
                'value' => 'estado.nombre',
                'contentOptions' => function($model){
                    if($model->id_estado == 1){
                        return ['style' => 'color:#F39C12; font-weight:bold; '];
                    }

                    if($model->id_estado == 2){
                        return ['style' => 'color:#00A65A; font-weight:bold; '];
                    }

                    if($model->id_estado == 3){
                        return ['style' => 'color:#336699; font-weight:bold; '];
                    }

                    if($model->id_estado == 4){
                        return ['style' => 'color:#DD4B39; font-weight:bold; '];
                    }

                    if($model->id_estado == 5){
                        return ['style' => 'color:#7300D4; font-weight:bold; '];
                    }
                },
                'headerOptions' => ['class' => 'gridViewEmpleadosHeader'],
            ],
            [
                'attribute' => 'fecha_registro',
                'label' => 'Fecha Registro',
                'value' => function ($data) {
                    return substr($data->fecha_registro,0,10); 
                },
                'contentOptions' => ['class' => 'gridViewEmpleados'],
                'headerOptions' => ['class' => 'gridViewEmpleadosHeader'],
            ],

            ['class' => 'yii\grid\ActionColumn',
            'contentOptions' => function($model){
                return ['style' => 'width:100px' ];
            }],
        ],
    ]); ?>


</div>
