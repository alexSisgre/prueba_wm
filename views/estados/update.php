<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Estados */

$this->title = 'Actualizar Estado: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Estados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar Estado';
?>

<div class="estados-update col-md-8 col-md-offset-2">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr/>
</div>

<div class="estados-update"> 
<?= $this->render('_form', [
    'model' => $model,
]) ?>

</div>

