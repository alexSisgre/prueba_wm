<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Estados */

$this->title = 'Crear Estados';
$this->params['breadcrumbs'][] = ['label' => 'Estados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="estados-create col-md-8 col-md-offset-2">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr/>
</div>

<div class="estados-create"> 
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>