<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Estados */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estados-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-6">  
                <?php
                    $var = [ 1 => 'Activo', 2 => 'Inactivo'];
                    echo $form->field($model, 'activo')->dropDownList($var, ['prompt' => '[-- Seleccione Opcion --]' ]); 
                ?>
            </div>    
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?= Html::submitButton('Guardar Estado', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>    
</div>

    

    <?php ActiveForm::end(); ?>

</div>
