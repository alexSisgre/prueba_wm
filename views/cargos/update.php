<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cargos */

$this->title = 'Actualizar Cargo:  ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Cargos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar Cargos';
?>
<div class="cargos-update col-md-8 col-md-offset-2">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr/>
</div>

<div class="cargos-update"> 
<?= $this->render('_form', [
    'model' => $model,
]) ?>

</div>
