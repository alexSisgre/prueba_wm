<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cargos */

$this->title = 'Crear Cargos';
$this->params['breadcrumbs'][] = ['label' => 'Cargos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cargos-create col-md-8 col-md-offset-2">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr/>
</div>

<div class="empleados-create"> 
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
