<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empleados".
 *
 * @property int $id
 * @property string $nombre
 * @property int $codigo
 * @property string $lugar_nacimiento
 * @property string $fecha_nacimiento
 * @property string $direccion
 * @property string $telefono
 * @property int $id_cargo
 * @property int $id_estado
 * @property string|null $fecha_registro
 *
 * @property Cargos $cargo
 * @property Estados $estado
 */
class Empleados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'codigo', 'lugar_nacimiento', 'fecha_nacimiento', 'direccion', 'telefono', 'id_cargo', 'id_estado'], 'required'],
            [['id_cargo', 'id_estado'], 'integer'],
            [['codigo'], 'integer', 'max' => 9999, 'min' => 0],
            [['telefono'], 'match', 'pattern' => '/[0-9].{4,15}/', 'message'=>'Telefono debe ser numerico, minimo 5 a 15 caracteres.'],
            [['lugar_nacimiento'], 'match', 'pattern' => '/[A-Za-z.,-].{1,49}/', 'message'=>'Lugar de nacimiento no puede contener numeros, minimo 2 a 50 caracteres.'],
            [['fecha_nacimiento', 'fecha_registro'], 'safe'],
            [['direccion'], 'string', 'max' => 255],
            [['nombre'], 'string', 'max' => 50, 'min' => 2],
            [['codigo'], 'unique'],
            [['id_cargo'], 'exist', 'skipOnError' => true, 'targetClass' => Cargos::className(), 'targetAttribute' => ['id_cargo' => 'id']],
            [['id_estado'], 'exist', 'skipOnError' => true, 'targetClass' => Estados::className(), 'targetAttribute' => ['id_estado' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'codigo' => 'Codigo',
            'lugar_nacimiento' => 'Lugar Nacimiento',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'id_cargo' => 'Id Cargo',
            'id_estado' => 'Id Estado',
            'fecha_registro' => 'Fecha Registro',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCargo()
    {
        return $this->hasOne(Cargos::className(), ['id' => 'id_cargo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstado()
    {
        return $this->hasOne(Estados::className(), ['id' => 'id_estado']);
    }
}
