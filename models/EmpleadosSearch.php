<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Empleados;

/**
 * EmpleadosSearch represents the model behind the search form of `app\models\Empleados`.
 */
class EmpleadosSearch extends Empleados
{
    /**
     * {@inheritdoc}
     */
    public $estado_nombre;
    public $cargo_nombre;

    public function rules()
    {
        return [
            [['id', 'codigo', 'id_cargo', 'id_estado'], 'integer'],
            [['nombre'],'string'],
            [['cargo_nombre','estado_nombre','lugar_nacimiento', 'fecha_nacimiento', 'direccion', 'telefono', 'fecha_registro'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Empleados::find();

        // Se hace join con tabla para relacionar campos
        $query->joinWith(['estado']);
        $query->joinWith(['cargo']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [ 'pageSize' => 4 ]
        ]);

        // Se crea el ordenamiento en el gridView de la tabla relacionada
        $dataProvider->sort->attributes['estado_nombre'] = [
            
            'asc' => ['estado.nombre' => SORT_ASC],
            'desc' => ['estado.nombre' => SORT_DESC],
        ];        

        // Se crea el ordenamiento en el gridView de la tabla relacionada
        $dataProvider->sort->attributes['cargo_nombre'] = [
            'asc' => ['cargo.nombre' => SORT_ASC],
            'desc' => ['cargo.nombre' => SORT_DESC],
        ];  

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'codigo' => $this->codigo,
            'nombre' => $this->nombre,
            'estado.nombre' => $this->nombre,
            'cargo.nombre' => $this->nombre,
            'fecha_nacimiento' => $this->fecha_nacimiento,
            'fecha_registro' => $this->fecha_registro,
        ]);

        $query->andFilterWhere(['like', 'lugar_nacimiento', $this->lugar_nacimiento])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'estados.nombre', $this->estado_nombre])
            ->andFilterWhere(['like', 'cargo.nombre', $this->estado_nombre])
            ->andFilterWhere(['like', 'telefono', $this->telefono]);

        return $dataProvider;
    }
}
